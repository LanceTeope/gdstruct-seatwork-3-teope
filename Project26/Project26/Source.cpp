#include <iostream>
#include <string>
#include<conio.h>

using namespace std;

void printRecursive(int from)
{
	if (from <= 0)
		return;

	cout << from << " ";
	printRecursive(from - 1);
}

int Add(int from)
{
	if (from == 0)
		return 0;

	return (from % 10 + Add(from / 10));
}// learned from geeksforgeeks


//int fib(int from)
//{
//	if (from <= 1)
//		return from;
//
//	return fib(from - 1) + fib(from - 2);
//}

int fib(int from)
{
	if (from == 1 || from == 0)
	{
		return from;
	}
	else
	{
		return fib(from - 1) + fib(from - 2);
	}
	
}


int prime(int from, int i)
{
	i = 2;
	if (from % i == 0)
	{
		return 1;
		cout << from << " is a Prime number" << endl;
	}
	else if (from % i != 0)
	{
		return 0;
		cout << from << "is not a Prime number" << endl;
	}

	prime(from + 1, i = 0);
}


void main()
{
	int num, i;
	i = 0;
	cout << "Input a number: ";
	cin >> num;
	cout << "=============================" << endl;
	cout << "Added values: " << Add(num) << endl;
	cout << "Fibonacci: ";
	while (i < num)
	{
		cout << " " << fib(i +1);
		i++;
	}
	cout << "\nPrime or not: ";
	prime(num, i);
	cout << endl;
	_getch();
}